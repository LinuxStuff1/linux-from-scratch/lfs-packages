# LFS 11 packages:

acl-2.3.1.tar.xz: [acl-2.3.1.tar.xz](packages/acl-2.3.1.tar.xz)

attr-2.5.1.tar.gz: [attr-2.5.1.tar.gz](packages/attr-2.5.1.tar.gz)

autoconf-2.71.tar.xz: [autoconf-2.71.tar.xz](packages/autoconf-2.71.tar.xz)

automake-1.16.4.tar.xz: [automake-1.16.4.tar.xz](packages/automake-1.16.4.tar.xz)

bash-5.1.8.tar.gz: [bash-5.1.8.tar.gz](packages/bash-5.1.8.tar.gz)

bc-5.0.0.tar.xz: [bc-5.0.0.tar.xz](packages/bc-5.0.0.tar.xz)

binutils-2.37.tar.xz: [binutils-2.37.tar.xz](packages/binutils-2.37.tar.xz)

binutils-2.37-upstream_fix-1.patch: [binutils-2.37-upstream_fix-1.patch](packages/binutils-2.37-upstream_fix-1.patch)

bison-3.7.6.tar.xz: [bison-3.7.6.tar.xz](packages/bison-3.7.6.tar.xz)

bzip2-1.0.8-install_docs-1.patch: [bzip2-1.0.8-install_docs-1.patch](packages/bzip2-1.0.8-install_docs-1.patch)

bzip2-1.0.8.tar.gz: [bzip2-1.0.8.tar.gz](packages/bzip2-1.0.8.tar.gz)

check-0.15.2.tar.gz: [check-0.15.2.tar.gz](packages/check-0.15.2.tar.gz)

coreutils-8.32-i18n-1.patch: [coreutils-8.32-i18n-1.patch](packages/coreutils-8.32-i18n-1.patch)

coreutils-8.32.tar.xz: [coreutils-8.32.tar.xz](packages/coreutils-8.32.tar.xz)

dbus-1.12.20.tar.gz: [dbus-1.12.20.tar.gz](packages/dbus-1.12.20.tar.gz)

dejagnu-1.6.3.tar.gz: [dejagnu-1.6.3.tar.gz](packages/dejagnu-1.6.3.tar.gz)

diffutils-3.8.tar.xz: [diffutils-3.8.tar.xz](packages/diffutils-3.8.tar.xz)

e2fsprogs-1.46.4.tar.gz: [e2fsprogs-1.46.4.tar.gz](packages/e2fsprogs-1.46.4.tar.gz)

elfutils-0.185.tar.bz2: [elfutils-0.185.tar.bz2](packages/elfutils-0.185.tar.bz2)

eudev-3.2.10.tar.gz: [eudev-3.2.10.tar.gz](packages/eudev-3.2.10.tar.gz)

expat-2.4.1.tar.xz: [expat-2.4.1.tar.xz](packages/expat-2.4.1.tar.xz)

expect5.45.4.tar.gz: [expect5.45.4.tar.gz](packages/expect5.45.4.tar.gz)

file-5.40.tar.gz: [file-5.40.tar.gz](packages/file-5.40.tar.gz)

findutils-4.8.0.tar.xz: [findutils-4.8.0.tar.xz](packages/findutils-4.8.0.tar.xz)

flex-2.6.4.tar.gz: [flex-2.6.4.tar.gz](packages/flex-2.6.4.tar.gz)

gawk-5.1.0.tar.xz: [gawk-5.1.0.tar.xz](packages/gawk-5.1.0.tar.xz)

gcc-11.2.0.tar.xz: [gcc-11.2.0.tar.xz](packages/gcc-11.2.0.tar.xz)

gdbm-1.20.tar.gz: [gdbm-1.20.tar.gz](packages/gdbm-1.20.tar.gz)

gettext-0.21.tar.xz: [gettext-0.21.tar.xz](packages/gettext-0.21.tar.xz)

glibc-2.34-fhs-1.patch: [glibc-2.34-fhs-1.patch](packages/glibc-2.34-fhs-1.patch)

glibc-2.34.tar.xz: [glibc-2.34.tar.xz](packages/glibc-2.34.tar.xz)

gmp-6.2.1.tar.xz: [gmp-6.2.1.tar.xz](packages/gmp-6.2.1.tar.xz)

gperf-3.1.tar.gz: [gperf-3.1.tar.gz](packages/gperf-3.1.tar.gz)

grep-3.7.tar.xz: [grep-3.7.tar.xz](packages/grep-3.7.tar.xz)

groff-1.22.4.tar.gz: [groff-1.22.4.tar.gz](packages/groff-1.22.4.tar.gz)

grub-2.06.tar.xz: [grub-2.06.tar.xz](packages/grub-2.06.tar.xz)

gzip-1.10.tar.xz: [gzip-1.10.tar.xz](packages/gzip-1.10.tar.xz)

iana-etc-20210611.tar.gz: [iana-etc-20210611.tar.gz](packages/iana-etc-20210611.tar.gz)

inetutils-2.1.tar.xz: [inetutils-2.1.tar.xz](packages/inetutils-2.1.tar.xz)

intltool-0.51.0.tar.gz: [intltool-0.51.0.tar.gz](packages/intltool-0.51.0.tar.gz)

iproute2-5.13.0.tar.xz: [iproute2-5.13.0.tar.xz](packages/iproute2-5.13.0.tar.xz)

Jinja2-3.0.1.tar.gz: [Jinja2-3.0.1.tar.gz](packages/Jinja2-3.0.1.tar.gz)

kbd-2.4.0-backspace-1.patch: [kbd-2.4.0-backspace-1.patch](packages/kbd-2.4.0-backspace-1.patch)

kbd-2.4.0.tar.xz: [kbd-2.4.0.tar.xz](packages/kbd-2.4.0.tar.xz)

kmod-29.tar.xz: [kmod-29.tar.xz](packages/kmod-29.tar.xz)

less-590.tar.gz: [less-590.tar.gz](packages/less-590.tar.gz)

lfs-bootscripts-20210608.tar.xz: [lfs-bootscripts-20210608.tar.xz](packages/lfs-bootscripts-20210608.tar.xz)

libcap-2.53.tar.xz: [libcap-2.53.tar.xz](packages/libcap-2.53.tar.xz)

libffi-3.4.2.tar.gz: [libffi-3.4.2.tar.gz](packages/libffi-3.4.2.tar.gz)

libpipeline-1.5.3.tar.gz: [libpipeline-1.5.3.tar.gz](packages/libpipeline-1.5.3.tar.gz)

libtool-2.4.6.tar.xz: [libtool-2.4.6.tar.xz](packages/libtool-2.4.6.tar.xz)

linux-5.13.12.tar.xz: [linux-5.13.12.tar.xz](packages/linux-5.13.12.tar.xz)

m4-1.4.19.tar.xz: [m4-1.4.19.tar.xz](packages/m4-1.4.19.tar.xz)

make-4.3.tar.gz: [make-4.3.tar.gz](packages/make-4.3.tar.gz)

man-db-2.9.4.tar.xz: [man-db-2.9.4.tar.xz](packages/man-db-2.9.4.tar.xz)

man-pages-5.13.tar.xz: [man-pages-5.13.tar.xz](packages/man-pages-5.13.tar.xz)

MarkupSafe-2.0.1.tar.gz: [MarkupSafe-2.0.1.tar.gz](packages/MarkupSafe-2.0.1.tar.gz)

meson-0.59.1.tar.gz: [meson-0.59.1.tar.gz](packages/meson-0.59.1.tar.gz)

mpc-1.2.1.tar.gz: [mpc-1.2.1.tar.gz](packages/mpc-1.2.1.tar.gz)

mpfr-4.1.0.tar.xz: [mpfr-4.1.0.tar.xz](packages/mpfr-4.1.0.tar.xz)

ncurses-6.2.tar.gz: [ncurses-6.2.tar.gz](packages/ncurses-6.2.tar.gz)

ninja-1.10.2.tar.gz: [ninja-1.10.2.tar.gz](packages/ninja-1.10.2.tar.gz)

openssl-1.1.1l.tar.gz: [openssl-1.1.1l.tar.gz](packages/openssl-1.1.1l.tar.gz)

patch-2.7.6.tar.xz: [patch-2.7.6.tar.xz](packages/patch-2.7.6.tar.xz)

perl-5.34.0.tar.xz: [perl-5.34.0.tar.xz](packages/perl-5.34.0.tar.xz)

perl-5.34.0-upstream_fixes-1.patch: [perl-5.34.0-upstream_fixes-1.patch](packages/perl-5.34.0-upstream_fixes-1.patch)

pkg-config-0.29.2.tar.gz: [pkg-config-0.29.2.tar.gz](packages/pkg-config-0.29.2.tar.gz)

procps-ng-3.3.17.tar.xz: [procps-ng-3.3.17.tar.xz](packages/procps-ng-3.3.17.tar.xz)

psmisc-23.4.tar.xz: [psmisc-23.4.tar.xz](packages/psmisc-23.4.tar.xz)

python-3.9.6-docs-html.tar.bz2: [python-3.9.6-docs-html.tar.bz2](packages/python-3.9.6-docs-html.tar.bz2)

Python-3.9.6.tar.xz: [Python-3.9.6.tar.xz](packages/Python-3.9.6.tar.xz)

readline-8.1.tar.gz: [readline-8.1.tar.gz](packages/readline-8.1.tar.gz)

README.md: [README.md](packages/README.md)

sed-4.8.tar.xz: [sed-4.8.tar.xz](packages/sed-4.8.tar.xz)

shadow-4.9.tar.xz: [shadow-4.9.tar.xz](packages/shadow-4.9.tar.xz)

sysklogd-1.5.1.tar.gz: [sysklogd-1.5.1.tar.gz](packages/sysklogd-1.5.1.tar.gz)

systemd-249.tar.gz: [systemd-249.tar.gz](packages/systemd-249.tar.gz)

systemd-249-upstream_fixes-1.patch: [systemd-249-upstream_fixes-1.patch](packages/systemd-249-upstream_fixes-1.patch)

systemd-man-pages-249.tar.xz: [systemd-man-pages-249.tar.xz](packages/systemd-man-pages-249.tar.xz)

sysvinit-2.99-consolidated-1.patch: [sysvinit-2.99-consolidated-1.patch](packages/sysvinit-2.99-consolidated-1.patch)

sysvinit-2.99.tar.xz: [sysvinit-2.99.tar.xz](packages/sysvinit-2.99.tar.xz)

tar-1.34.tar.xz: [tar-1.34.tar.xz](packages/tar-1.34.tar.xz)

tcl8.6.11-html.tar.gz: [tcl8.6.11-html.tar.gz](packages/tcl8.6.11-html.tar.gz)

tcl8.6.11-src.tar.gz: [tcl8.6.11-src.tar.gz](packages/tcl8.6.11-src.tar.gz)

texinfo-6.8.tar.xz: [texinfo-6.8.tar.xz](packages/texinfo-6.8.tar.xz)

tzdata2021a.tar.gz: [tzdata2021a.tar.gz](packages/tzdata2021a.tar.gz)

udev-lfs-20171102.tar.xz: [udev-lfs-20171102.tar.xz](packages/udev-lfs-20171102.tar.xz)

util-linux-2.37.2.tar.xz: [util-linux-2.37.2.tar.xz](packages/util-linux-2.37.2.tar.xz)

vim-8.2.3337.tar.gz: [vim-8.2.3337.tar.gz](packages/vim-8.2.3337.tar.gz)

XML-Parser-2.46.tar.gz: [XML-Parser-2.46.tar.gz](packages/XML-Parser-2.46.tar.gz)

xz-5.2.5.tar.xz: [xz-5.2.5.tar.xz](packages/xz-5.2.5.tar.xz)

zlib-1.2.11.tar.xz: [zlib-1.2.11.tar.xz](packages/zlib-1.2.11.tar.xz)

zstd-1.5.0.tar.gz: [zstd-1.5.0.tar.gz](packages/zstd-1.5.0.tar.gz)
